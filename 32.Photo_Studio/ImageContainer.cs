﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Drawing.Printing;

namespace _32.Photo_Studio
{
    public partial class ImageContainer : Form
    {
        public Immagine Immagine { get; set; }

        public ImageContainer(Immagine _immagine)
        {
            Immagine = _immagine;
            InitializeComponent();
            refreshForm();
            setWidth();
        }

        public void refreshForm()
        {
            annullaToolStripMenuItem.Enabled = Immagine.CanAnnullare;
            ripristinaToolStripMenuItem.Enabled = Immagine.CanRipetere;
            pictBox_img.Image = Immagine.Image;
            this.Text = Immagine.Nome;
        }

        public void setWidth()
        {
            //pictBox.width : pictBox.height = foto.width : fot.height
            int newPictBoxWidth = ((this.Height - 64) * Immagine.Image.Width) / Immagine.Image.Height;
            this.Size = new Size(newPictBoxWidth + 16, this.Height);
        }

        private void ImageContainer_Resize(object sender, EventArgs e)
        {
            setWidth();
        }

        #region effetti

        private void scalaDiGrigiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Immagine.applicaEffetto(Effetti.scalaDiGrigi, null);
            refreshForm();
        }

        private void negativoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Immagine.applicaEffetto(Effetti.negativo, null);
            refreshForm();
        }

        private void bilanciamentoColoriToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RGBSelector rgbSelector = new RGBSelector();
            rgbSelector.Text = "Bilanciamento Colori";
            rgbSelector.Parent = this;
            rgbSelector.effetto = Effetti.bilanciamentoColori;
            rgbSelector.refreshUndoRedo();
            rgbSelector.ShowDialog();
        }

        private void luminositaToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            SingleSelector singleSelector = new SingleSelector();
            singleSelector.Text = "Luminosità";
            singleSelector.Parent = this;
            singleSelector.effetto = Effetti.luminosita;
            singleSelector.refreshUndoRedo();
            singleSelector.ShowDialog();
        }

        private void seppiaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Immagine.applicaEffetto(Effetti.seppia, null);
            refreshForm();
        }

        private void sogliaturaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SingleSelector singleSelector = new SingleSelector();
            singleSelector.Text = "Sogliatura";
            singleSelector.Parent = this;
            singleSelector.effetto = Effetti.sogliatura;
            singleSelector.refreshUndoRedo();

            singleSelector.nUpDw.Value = 128;
            singleSelector.trkBar.Value = 128;

            singleSelector.nUpDw.Minimum = 0;
            singleSelector.trkBar.Minimum = 0;

            singleSelector.ShowDialog();
        }

        #endregion

        #region modifica

        public void annullaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Immagine.annullaModifica();
            refreshForm();
        }

        public void ripristinaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Immagine.ripetiModifica();
            refreshForm();
        }

        #endregion


        #region file

        private void salvaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (File.Exists(Immagine.Path))
                File.Delete(Immagine.Path);

            Immagine.Image.Save(Immagine.Path);
        }

        private void salvaconnomeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Image File|*.png|Image File|*.jpg|Image File|*.jpeg|Image File|*.bmp";
            saveFileDialog1.Title = "Salva immagine";
            saveFileDialog1.RestoreDirectory = true;
            DialogResult dialogResult = saveFileDialog1.ShowDialog();
            if (saveFileDialog1.FileName != "" && dialogResult == DialogResult.OK)
            {
                if (File.Exists(saveFileDialog1.FileName))
                    File.Delete(saveFileDialog1.FileName);

                Immagine.Image.Save(saveFileDialog1.FileName);
                Immagine.Path = saveFileDialog1.FileName;
                refreshForm();
            }

        }

        private void stampaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PrintDocument printDocument1 = new PrintDocument();
            printDocument1.PrintPage += (_, args) =>
            {
                Image i = Immagine.Image;
                float newWidth = i.Width * 100 / i.HorizontalResolution;
                float newHeight = i.Height * 100 / i.VerticalResolution;

                float widthFactor = newWidth / args.MarginBounds.Width;
                float heightFactor = newHeight / args.MarginBounds.Height;

                if (widthFactor > 1 | heightFactor > 1)
                {
                    if (widthFactor > heightFactor)
                    {
                        newWidth = newWidth / widthFactor;
                        newHeight = newHeight / widthFactor;
                    }
                    else
                    {
                        newWidth = newWidth / heightFactor;
                        newHeight = newHeight / heightFactor;
                    }
                }
                args.Graphics.DrawImage(i, 10, 10, (int)newWidth, (int)newHeight);
            };

            printDialog1.Document = printDocument1;
            printDialog1.AllowPrintToFile = true;
            printDialog1.AllowSelection = true;
            printDialog1.AllowSomePages = true;
            printDialog1.PrintToFile = true;
            if (printDialog1.ShowDialog() == DialogResult.OK)
                printDocument1.Print();
        }

        #endregion
    }
}
