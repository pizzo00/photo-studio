﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace _32.Photo_Studio
{
    public static class Effetti
    {
        public delegate Color Effetto(Color input, object parameter);

        public static Color negativo(Color input, object _)
        {
            return Color.FromArgb(input.A, (255 - input.R), (255 - input.G), (255 - input.B));
        }

        public static Color scalaDiGrigi(Color input, object _)
        {
            int grayScale = (int)((input.R * 0.3) + (input.G * 0.59) + (input.B * 0.11));

            if (grayScale > 255) { grayScale = 255; }
            else if (grayScale < 0) { grayScale = 0; }

            return Color.FromArgb(input.A, grayScale, grayScale, grayScale);
        }

        public static Color bilanciamentoColori(Color input, object colorGain)
        {
            int alphaGain = (colorGain as int[])[0];
            int redGain = (colorGain as int[])[1];
            int greenGain = (colorGain as int[])[2];
            int blueGain = (colorGain as int[])[3];

            int alpha = input.A + alphaGain;
            int red = input.R + redGain;
            int green = input.G + greenGain;
            int blue = input.B + blueGain;

            if (alpha > 255) { alpha = 255; }
            else if (alpha < 0) { alpha = 0; }

            if (red > 255) { red = 255; }
            else if (red < 0) { red = 0; }

            if (green > 255) { green = 255; }
            else if (green < 0) { green = 0; }

            if (blue > 255) { blue = 255; }
            else if (blue < 0) { blue = 0; }

            return Color.FromArgb(alpha, red, green, blue);
        }

        public static Color luminosita(Color input, object gain)
        {
            float red = input.R + (int)gain;
            float green = input.G + (int)gain;
            float blue = input.B + (int)gain;

            if (red > 255) { red = 255; }
            else if (red < 0) { red = 0; }

            if (green > 255) { green = 255; }
            else if (green < 0) { green = 0; }

            if (blue > 255) { blue = 255; }
            else if (blue < 0) { blue = 0; }


            return Color.FromArgb(input.A, (int)red, (int)green, (int)blue);
        }

        public static Color seppia(Color input, object _)
        {
            int red     = (int)((input.R * 0.393) + (input.G * 0.769) + (input.B * 0.189));
            int green   = (int)((input.R * 0.349) + (input.G * 0.686) + (input.B * 0.168));
            int blue    = (int)((input.R * 0.272) + (input.G * 0.534) + (input.B * 0.131));

            if (red > 255) { red = 255; }
            else if (red < 0) { red = 0; }

            if (green > 255) { green = 255; }
            else if (green < 0) { green = 0; }

            if (blue > 255) { blue = 255; }
            else if (blue < 0) { blue = 0; }

            return Color.FromArgb(input.A, red, green, blue);
        }

        public static Color sogliatura(Color input, object soglia)
        {
            int value = (input.R + input.G + input.B) / 3;

            if(value >= (int)soglia)
            {
                return Color.FromArgb(input.A, 255, 255, 255);
            }
            else
            {
                return Color.FromArgb(input.A, 0, 0, 0);
            }
        }

    }
}
