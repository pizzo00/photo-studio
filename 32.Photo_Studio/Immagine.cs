﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;

namespace _32.Photo_Studio
{
    public class Immagine
    {
        public Bitmap Image { get; set; }
        public string Nome { get => System.IO.Path.GetFileName(Path); }
        public string Path { get; set; }
        public bool CanAnnullare { get => (versioniPrecedenti.Count != 0); }
        public bool CanRipetere { get => (versioniAnnullate.Count != 0); }

        private Stack<Bitmap> versioniPrecedenti;
        private Stack<Bitmap> versioniAnnullate;

        public Immagine()
        {
            Image = null;
            Path = "";
            versioniPrecedenti = new Stack<Bitmap>();
            versioniAnnullate = new Stack<Bitmap>();
        }

        public Immagine(Bitmap _image, string _path)
        {
            Image = _image;
            Path = _path;
            versioniPrecedenti = new Stack<Bitmap>();
            versioniAnnullate = new Stack<Bitmap>();
        }

        public void applicaEffetto(Effetti.Effetto effetto, object parameter)
        {
            versioniPrecedenti.Push(Image.Clone() as Bitmap);
            versioniAnnullate = new Stack<Bitmap>();
            Image = ImageProcessor.processImage(Image, effetto, parameter);
        }

        public void annullaModifica()
        {
            if(this.CanAnnullare)
            {
                versioniAnnullate.Push(this.Image.Clone() as Bitmap);
                this.Image = versioniPrecedenti.Pop();
            }
        }

        public void ripetiModifica()
        {
            if (this.CanRipetere)
            {
                versioniPrecedenti.Push(this.Image.Clone() as Bitmap);
                this.Image = versioniAnnullate.Pop();
            }
        }
    }
}
