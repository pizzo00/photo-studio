﻿namespace _32.Photo_Studio
{
    partial class SingleSelector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.trkBar = new System.Windows.Forms.TrackBar();
            this.nUpDw = new System.Windows.Forms.NumericUpDown();
            this.btt_applica = new System.Windows.Forms.Button();
            this.btt_redo = new System.Windows.Forms.PictureBox();
            this.btt_undo = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.trkBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUpDw)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btt_redo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btt_undo)).BeginInit();
            this.SuspendLayout();
            // 
            // trkBar
            // 
            this.trkBar.Location = new System.Drawing.Point(12, 39);
            this.trkBar.Maximum = 255;
            this.trkBar.Minimum = -255;
            this.trkBar.Name = "trkBar";
            this.trkBar.Size = new System.Drawing.Size(260, 45);
            this.trkBar.TabIndex = 0;
            this.trkBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trkBar.Scroll += new System.EventHandler(this.trkBar_red_Scroll);
            // 
            // nUpDw
            // 
            this.nUpDw.Location = new System.Drawing.Point(278, 39);
            this.nUpDw.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nUpDw.Minimum = new decimal(new int[] {
            255,
            0,
            0,
            -2147483648});
            this.nUpDw.Name = "nUpDw";
            this.nUpDw.Size = new System.Drawing.Size(57, 20);
            this.nUpDw.TabIndex = 1;
            this.nUpDw.ValueChanged += new System.EventHandler(this.nUpDw_red_ValueChanged);
            // 
            // btt_applica
            // 
            this.btt_applica.Location = new System.Drawing.Point(260, 90);
            this.btt_applica.Name = "btt_applica";
            this.btt_applica.Size = new System.Drawing.Size(75, 23);
            this.btt_applica.TabIndex = 2;
            this.btt_applica.Text = "Applica";
            this.btt_applica.UseVisualStyleBackColor = true;
            this.btt_applica.Click += new System.EventHandler(this.btt_applica_Click);
            // 
            // btt_redo
            // 
            this.btt_redo.Image = global::_32.Photo_Studio.Properties.Resources.redo;
            this.btt_redo.Location = new System.Drawing.Point(154, 78);
            this.btt_redo.Name = "btt_redo";
            this.btt_redo.Size = new System.Drawing.Size(35, 35);
            this.btt_redo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btt_redo.TabIndex = 16;
            this.btt_redo.TabStop = false;
            this.btt_redo.Visible = false;
            this.btt_redo.Click += new System.EventHandler(this.btt_redo_Click);
            // 
            // btt_undo
            // 
            this.btt_undo.Image = global::_32.Photo_Studio.Properties.Resources.undo;
            this.btt_undo.Location = new System.Drawing.Point(94, 78);
            this.btt_undo.Name = "btt_undo";
            this.btt_undo.Size = new System.Drawing.Size(35, 35);
            this.btt_undo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btt_undo.TabIndex = 15;
            this.btt_undo.TabStop = false;
            this.btt_undo.Visible = false;
            this.btt_undo.Click += new System.EventHandler(this.btt_undo_Click);
            // 
            // SingleSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(347, 125);
            this.Controls.Add(this.btt_redo);
            this.Controls.Add(this.btt_undo);
            this.Controls.Add(this.btt_applica);
            this.Controls.Add(this.nUpDw);
            this.Controls.Add(this.trkBar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "SingleSelector";
            this.Text = "Single Selector";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.trkBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUpDw)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btt_redo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btt_undo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btt_applica;
        private System.Windows.Forms.PictureBox btt_redo;
        private System.Windows.Forms.PictureBox btt_undo;
        public System.Windows.Forms.TrackBar trkBar;
        public System.Windows.Forms.NumericUpDown nUpDw;
    }
}