﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _32.Photo_Studio
{
    public partial class SingleSelector : Form
    {
        public ImageContainer Parent { get; set; }

        public Effetti.Effetto effetto { get; set; }

        public SingleSelector()
        {
            InitializeComponent();
        }

        private void btt_applica_Click(object sender, EventArgs e)
        {
            Parent.Immagine.applicaEffetto(effetto, (int)nUpDw.Value);
            Parent.refreshForm();
            refreshUndoRedo();
        }

        private void trkBar_red_Scroll(object sender, EventArgs e)
        {
            nUpDw.Value = trkBar.Value;
        }

        private void nUpDw_red_ValueChanged(object sender, EventArgs e)
        {
            trkBar.Value = (int)nUpDw.Value;
        }

        private void btt_undo_Click(object sender, EventArgs e)
        {
            Parent.annullaToolStripMenuItem_Click(null, null);
            refreshUndoRedo();
        }

        private void btt_redo_Click(object sender, EventArgs e)
        {
            Parent.ripristinaToolStripMenuItem_Click(null, null);
            refreshUndoRedo();
        }

        public void refreshUndoRedo()
        {
            btt_undo.Enabled = Parent.Immagine.CanAnnullare;
            btt_redo.Enabled = Parent.Immagine.CanRipetere;

            btt_undo.Visible = Parent.Immagine.CanAnnullare;
            btt_redo.Visible = Parent.Immagine.CanRipetere;
        }
    }
}
